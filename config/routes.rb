Rails.application.routes.draw do
  root to: "pages#home"

  mount ActionCable.server => '/cable'
  namespace :api, defaults: {format: :json} do
    resources :quotes, only: [:show]
  end
end
