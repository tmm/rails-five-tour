declare type Quote = {
  text: string,
  author: string,
  previous_id: number,
  next_id: number,
};

