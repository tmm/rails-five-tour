class QuoteChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'quote:quote'
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
