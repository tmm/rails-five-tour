(function() {
  App.quote = App.cable.subscriptions.create("QuoteChannel", {
    connected: function()  {
      console.log('Connected');
    },
    disconnected: function() {
      console.log('Disconnected');
    },
    received: function(data) {
      console.log(data);
      quoteStream.next(data);
    }
  });
}).call(this);
