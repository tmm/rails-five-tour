class Quote < ApplicationRecord
  after_save :do_broadcast

  def next_id
    self.class.where('id > ?', self.id).pluck(:id).first
  end

  def previous_id
    self.class.where('id < ?', self.id).pluck(:id).last
  end

  private

  def do_broadcast
    QuoteChannel.broadcast_to 'quote', self.as_json
  end
end
