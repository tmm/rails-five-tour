// @flow

import React from 'react';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
import 'typeface-roboto';
import 'material-design-icons/iconfont/material-icons.css';
import Reboot from 'material-ui/Reboot';
import {MuiThemeProvider, createMuiTheme} from 'material-ui/styles';

import QuotesDisplay from './QuotesDisplay';

export const aBlue = {
  '50': '#e6eaed',
  '100': '#c2cbd3',
  '200': '#99a8b5',
  '300': '#708597',
  '400': '#516a81',
  '500': '#32506b',
  '600': '#2d4963',
  '700': '#264058',
  '800': '#1f374e',
  '900': '#13273c',
  'A100': '#7ab7ff',
  'A200': '#479bff',
  'A400': '#147fff',
  'A700': '#0072f9',
  'contrastDefaultColor': 'light',
};

export const aPurple = {
  '50': '#eeebfa',
  '100': '#d6cdf2',
  '200': '#baace9',
  '300': '#9e8ae0',
  '400': '#8a71d9',
  '500': '#7558d2',
  '600': '#6d50cd',
  '700': '#6247c7',
  '800': '#583dc1',
  '900': '#452db6',
  'A100': '#f9f8ff',
  'A200': '#cfc5ff',
  'A400': '#a492ff',
  'A700': '#8e79ff',
  'contrastDefaultColor': 'light',
};

const theme = createMuiTheme({
  palette: {
    primary: aBlue,
    secondary: aPurple,
    type: 'light'
  },
});

type AppProps = {
  startingQuoteId: number,
};

const App = (props: AppProps) => (
  <MuiThemeProvider theme={theme}>
    <Reboot />
    <Router startingQuoteId={props.startingQuoteId}>
      <Route path='/' startingQuoteId={props.startingQuoteId}
        render={(routeProps) => <QuotesDisplay {...props} {...routeProps} />}
      />
    </Router>
  </MuiThemeProvider>
)

export default App
