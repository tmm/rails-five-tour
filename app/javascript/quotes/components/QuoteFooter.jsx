// @flow

import React from 'react';
import {Link} from 'react-router-dom';
import BottomNavigation,
  {BottomNavigationAction} from 'material-ui/BottomNavigation';
import HomeIcon from 'material-ui-icons/Home';

type Props = {
  startingQuoteId: number,
};

const QuoteFooter = ({startingQuoteId}: Props) => (
  <BottomNavigation showLabels>
    <BottomNavigationAction
      label='Back to Beginning'
      icon={<HomeIcon />}
      component={Link}
      to={`/?quote=${startingQuoteId}`}
    />
  </BottomNavigation>
);

export default QuoteFooter;
