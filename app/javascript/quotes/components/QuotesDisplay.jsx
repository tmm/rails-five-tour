// @flow

import React from 'react';
import {
  Link,
  Redirect,
  type RouterHistory,
  type Location
} from 'react-router-dom';
import queryString from 'query-string';
import axios from 'axios';
import QuoteText from './QuoteText';
import QuoteNavigation from './QuoteNavigation';
import QuoteFooter from './QuoteFooter';
import AppBar from 'material-ui/AppBar';
import Button from 'material-ui/Button';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Snackbar from 'material-ui/Snackbar';

type Props = {
  startingQuoteId: number,
  history: RouterHistory,
  location: Location,
};

type State = {
  quote: Quote,
  errorMessage: ?string,
  wsQuote: boolean,
  fireRedirect: boolean,
};

class QuotesDisplay extends React.Component<Props, State> {
  state = {
    wsQuote: false,
    fireRedirect: false,
    errorMessage: null,
    quote: {
      text: '',
      author: '',
      previous_id: -1,
      next_id: -1,
    },
  };

  qsParams = null;
  quoteId = 0;
  stream: rxjs$Subject<Quote>;

  fetchQuote(id: number) {
    axios.get(`api/quotes/${id}`)
      .then(response => {
        this.setState({
          quote: response.data,
          fireRedirect: false,
          errorMessage: null
        });
      })
      .catch(error => {
        if(error.response.status === 404) {
          this.setState({errorMessage: `Could not find quote with id ${id}`});
        } else {
          this.setState({errorMessage: null, fireRedirect: true});
        }
      });
  }

  setQuoteIdFromQueryString(qs: string) {
    this.qsParams = queryString.parse(qs);
    if(this.qsParams.quote) {
      this.quoteId = Number(this.qsParams.quote);
    } else {
      this.quoteId = this.props.startingQuoteId;
      this.props.history.push(`/?quote=${this.quoteId}`);
    }
  }

  componentDidMount() {
    this.setQuoteIdFromQueryString(this.props.location.search);
    this.fetchQuote(this.quoteId);
    this.stream = window.quoteStream;
    this.stream.subscribe(s => {
      this.props.history.push(`/?quote=${s.id}`);
    });
  }

  componentWillUnmount() {
    this.stream.unsubscribe();
  }

  componentWillReceiveProps(nextProps: Props) {
    if(this.state.wsQuote) {
      return;
    }
    this.setQuoteIdFromQueryString(nextProps.location.search);
    this.fetchQuote(this.quoteId);
  }

  resetId() {
    this.props.history.goBack();
  }

  render() {
    const {previous_id, next_id, text, author} = this.state.quote;

    const style = {
      container: {
        display: 'grid',
        gridTemplateRows: '68px 1fr 68px',
        gridTemplateColumns: '1fr 80px 400px 80px 1fr',
        gridColumnGap: 15,
        gridRowGap: 15,
        justifyItems: 'center',
        alignItems: 'center',
        minHeight: '100vh',
        opacity: 0.7
      },
      leftNav: {
        gridRow: 2,
        gridColumn: 2
      },
      rightNav: {
        gridRow: 2,
        gridColumn: 4
      },
      quote: {
        gridRow: 2,
        gridColumn: 3
      },
      footer: {
        gridRow: 3,
        gridColumn: '1 / -1',
        justifySelf: 'stretch',
        alignSelf: 'end'
      }
    };

    const action = (
      <Button color='accent' dense onClick={() => this.resetId()}>
        Go back
      </Button>
    );

    return (
      <div style={style.container}>
        <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'center'}}
          open={!!this.state.errorMessage} message={this.state.errorMessage}
          action={action} />
        <AppBar>
          <Toolbar>
            <Typography type='title' color='inherit'>
              Material UI Quote Browser
            </Typography>
          </Toolbar>
        </AppBar>
        {this.state.fireRedirect && <Redirect to='/' />}
        <QuoteNavigation style={style.leftNav} direction='previous'
          to={previous_id} />
        <div style={style.quote}>
          <QuoteText quote={this.state.quote} />
        </div>
        <QuoteNavigation style={style.rightNav} direction='next'
          to={next_id} />
        <div style={style.footer}>
          <QuoteFooter startingQuoteId={this.props.startingQuoteId} />
        </div>
      </div>
    );
  }
}

export default QuotesDisplay;
