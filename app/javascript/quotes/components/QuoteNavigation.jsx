// @flow

import React from 'react';
import {Link} from 'react-router-dom';
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';

type Props = {
  style: Object,
  direction: string,
  to: number,
};

const QuoteNavigation = ({style, direction, to}: Props) => {
  if(!to) {
    return <div />;
  }

  const angle = direction === 'previous'
    ? 'navigate_before' : 'navigate_next';

  return (
    <div style={style}>
      <Link to={`/?quote=${to}`}>
        <Button fab color='accent' aria-label='Previous'>
          <Icon>{angle}</Icon>
        </Button>
      </Link>
    </div>
  );
};

export default QuoteNavigation;
