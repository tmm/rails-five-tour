// @flow

import React from 'react';
import Card, {CardContent} from 'material-ui/Card';
import Typography from 'material-ui/Typography';

type Props = {
  quote: Quote,
};

const QuoteText = ({quote}: Props) => (
  <Card>
    <CardContent>
      <Typography type='headline'>{quote.text}</Typography>
      <Typography type='subheading' color='secondary'>
        — {quote.author}
      </Typography>
    </CardContent>
  </Card>
);

export default QuoteText;
