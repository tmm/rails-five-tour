// @flow

import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import {Subject} from 'rxjs';

window.quoteStream = new Subject();

const quotes: ?HTMLElement = document.querySelector('#quotes');

if(quotes != null) {
  ReactDOM.render(
    <App startingQuoteId={Number(quotes.dataset.startingQuoteId)} />,
    quotes
  );
} else {
  throw new Error("Could not find mountpoint #quotes");
}

